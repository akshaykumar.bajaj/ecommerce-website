import React, { Component } from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import axios from 'axios';
import './App.css';

import AllProducts from './Components/AllProducts';
import Footer from './Components/Footer';
import Header from './Components/Header';
import SingleProductPage from './Components/SingleProductPage';
import Loader from './Components/Loader';

export class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      hasUsersLoaded: false,
      loggedIn: false,
      cartDetails: [],
    }
  }

  componentDidMount() {
    axios.get('https://fakestoreapi.com/users')
      .then((res) => {
        this.setState({
          hasUsersLoaded: true,

          users: res.data,
        });
      })
      .catch(error => console.error(error))

  }
 render() {
    return (
      this.state.hasUsersLoaded ?
        <div className="App">
          <Router>
            <Header cartDetails={this.state.cartDetails} increaseItem={this.increaseItem} decreaseItem={this.decreaseItem} login={this.login} logout = {this.logout}/>
            <Switch>
              <Route exact path="/" component={AllProducts} />
              <Route path="/productPage/:id" render={(props) =>
                <SingleProductPage {...props} productsInCart={this.state.cartDetails}
                  addToCart={this.addItemToCart}
                  increaseItem={this.increaseItem}
                  decreaseItem={this.decreaseItem}
                />}
              />
            </Switch>
            <Footer />
          </Router>
        </div>
        : <Loader />
    )
  }

  login = (submitedDetails) => {
    let user = this.state.users.find(user => user.username === submitedDetails.username)

    if (user && user.password === submitedDetails.password) {
      axios.get(`https://fakestoreapi.com/carts/${user.id}`)
        .then((res) => {
          this.setState({
            ...this.state,
            cartDetails: res.data.products,
          });
        })
        .catch(error => console.error(error))
        return user
    }
    else{
      return false
    }
  }
  
  logout = () => {
    this.setState({
      cartDetails: [],
    })
  }
  addItemToCart = (newItem) => {
    let products = this.state.cartDetails;
    products.push(newItem);

    this.setState({
      cartDetails: products,
    })
  }

  increaseItem = (id) => {
    let products = this.state.cartDetails;
    if (products.some((cartItem) => parseInt(cartItem.productId) === id)) {
      products = products.map((cartItem) => {
        if (cartItem.productId === Number(id)) {
          cartItem.quantity += 1;
        }
        return cartItem;
      })
    }
    this.setState({
      cartDetails: products,
    })
  }

  decreaseItem = (id) => {
    let products = this.state.cartDetails;

    let currentProduct = products.find(product => product.productId === Number(id));
    if (currentProduct.quantity > 1) {
      products = products.map((cartItem) => {
        if (cartItem.productId === Number(id)) {
          cartItem.quantity -= 1;
        }
        return cartItem;
      })
    }
    else {
      products = products.filter(product => product.productId !== Number(id))
    }
    this.setState({
      cartDetails: products,
    })
  }

}

export default App