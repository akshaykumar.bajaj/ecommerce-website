import React from 'react'

function QunatityBar(props) {
    return (
        <div>
            <div className="btn-group" role="group" aria-label="Basic mixed styles example">
                <button type="button" className="btn btn-danger"  onClick ={decreaseItem}>-</button>
                <button type="button" className="btn btn-warning btn-md" disabled>{props.product.quantity}</button>
                <button type="button" className="btn btn-success" onClick ={increaseItem}>+</button>
            </div>
        </div>
    )
    function increaseItem(){
        props.increaseItem(props.product.productId);
    }
    function decreaseItem(){
        props.decreaseItem(props.product.productId);
    }
}
export default QunatityBar
