import React, { Component } from 'react'

export class Login extends Component {
    constructor(props) {
        super(props)

        this.state = {
            loggedIn: false,
            passwordInvalid: false,
        }
    }

    loginSumbit = (event) => {
        event.preventDefault()
        let loginDetails = {
            username: event.target.username.value,
            password: event.target.password.value,
        }
        let loggedInAs = this.props.login(loginDetails);
        if(!loggedInAs)
        {
            this.setState({
                passwordInvalid: true,
            })
        }
        else{
            this.setState({
                passwordInvalid: true,
            })
        }
        this.setState({
            loggedIn: loggedInAs,
        })
    }

    logOut = () => {
        this.setState({
            loggedIn: false,
            passwordInvalid: false,
        })
        this.props.logout();
    } 
    render() {
        return (
            < div >
                <div class="modal fade  text-black" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Login</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            {this.state.loggedIn ?
                                (<div>
                                    <div class="modal-body">
                                        Hello, {this.state.loggedIn.name.firstname}!
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                        <button class="btn btn-primary" type="button" onClick={this.logOut}>Logout</button>
                                    </div>
                                </div>)
                                :
                                (<form onSubmit={this.loginSumbit}>
                                    <div class="modal-body">
                                        <div class="form-floating mb-3">
                                            <input type="text" class="form-control" id="floatingInput" name="username" required />
                                            <label for="floatingInput">Email address</label>
                                        </div>
                                        <div class="form-floating">
                                            <input type="password" class="form-control" id="floatingPassword" name="password" required />
                                            <label for="floatingPassword">Password</label>
                                            {this.state.passwordInvalid && <div className="text-danger">Invalid Pass </div>}
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                        <button class="btn btn-primary" type="submit">Login</button>
                                    </div>
                                </form>)}
                        </div>
                    </div>
                </div>
            </div >

        )
    }
}

export default Login
