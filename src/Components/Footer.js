import React from 'react'
import facebookIcon from "../icons/icon-facebook.svg"
import instaIcon from  "../icons/icon-instagram.svg"
import twitterIcon from "../icons/icon-twitter.svg"
import youtubeIcon from "../icons/icon-youtube.svg"


function Footer() {
    return (
        <div className="footer d-flex flex-column flex-md-row p-2">  
        <div className="d-flex flex-md-row">
            <div className="nav flex-column">
              <a className ="nav-link text-decration-none text-white" href="/">About Us</a>
              <a className ="nav-link text-decration-none text-white" href="/">Shipping Policy</a>
              <a className ="nav-link text-decration-none text-white" href="/">Return Policy</a>
            </div>
            <div className ="nav flex-column">
              <a className ="nav-link text-decration-none text-white" href="/">Careers</a>
              <a className ="nav-link text-decration-none text-white" href="/">Support</a>
              <a className ="nav-link text-decration-none text-white" href="/">Privacy Policy</a>
            </div>
        </div>
        <div>
            <div className="d-flex justify-content-between gap-5">
            <a href="/"><img src={facebookIcon} alt="facebook" /></a>
            <a href="/"><img src={instaIcon} alt="insta" /></a>
            <a href="/"><img src= {twitterIcon} alt="icon-twitter" /></a>
            <a href="/"><img src={youtubeIcon} alt="icon-youtube" /></a></div>
        </div>     
        </div>
    )
}

export default Footer


