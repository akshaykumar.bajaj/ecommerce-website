import React, { Component } from 'react'
import axios from 'axios'
import Loader from './Loader'
import ErrorPage from './ErrorPage'
import QunatityBar from './QunatityBar'

export class CartItem extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isLoading: true,
            isError: false,
        }
    }
    componentDidMount() {
        axios.get(`https://fakestoreapi.com/products/${this.props.product.productId}`)
            .then((res) => this.setState({
                productDetails: res.data,
                isLoading: false,
            }))
            .catch((error) => {
                console.log(error)
                this.setState({
                    isLoading: false,
                    isError: true,
                })
            })
    }
    render() {
        return (
            this.state.isLoading ? <Loader /> :
                this.state.isError ? <ErrorPage /> : <div>
                    <div className="card text-black m-2">
                        <div className="row">
                            <div className="col-4 align-self-center">
                                <img src={this.state.productDetails.image} className="img-fluid rounded-start" alt="productImage" />
                            </div>
                            <div className="card-body col-8 ">
                                <h5 className="card-title text-muted">{this.state.productDetails.title}</h5>
                                <QunatityBar product = {this.props.product} increaseItem ={this.props.increaseItem} decreaseItem={this.props.decreaseItem}/>
                            </div>
                        </div>
                    </div>
                </div>
        )
    }
}

export default CartItem
