import React, { Component } from 'react'
import axios from 'axios'

import staricon from '../star.svg'

import Loader from './Loader'
import ErrorPage from './ErrorPage'
import QunatityBar from './QunatityBar'

export class SingleProductPage extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isLoading: true,
            isError: false,
        }
    }
    componentDidMount() {
        axios.get(`https://fakestoreapi.com/products/${this.props.match.params.id}`)
            .then((res) => this.setState({
                productDetails: res.data,
                isLoading: false,
            }))
            .catch((error) => {
                console.log(error)
                this.setState({
                    isLoading: false,
                    isError: true,
                })
            })
    }
    inCartDetails = Array.isArray(this.props.productsInCart) ?
        this.props.productsInCart.find(product => product.productId === parseInt(this.props.match.params.id)) : false

    checkInCart = () => {
        return this.props.productsInCart.find(product => product.productId === parseInt(this.props.match.params.id))
    }

    addToCart = () => {
        let inCartDetails = {
            productId: parseInt(this.props.match.params.id),
            quantity: 1,
        }
        this.props.addToCart(inCartDetails)
    }

    render() {
        return (
            this.state.isLoading ? <Loader /> :
                this.state.isError ? <ErrorPage /> : <div className="container">
                    <div className="row" >
                        <div className="col-lg-4  offset-lg-2 
                                        col-md-4  offset-md-2 
                                        col-sm-8  offset-sm-2
                                        col-6  offset-3
                                        d-flex flex-column align-items-center">
                            <img className="rounded img-thumbnail m-3" src={this.state.productDetails.image} alt="productImage" />
                            {this.checkInCart() ? <QunatityBar product={this.checkInCart()} increaseItem={this.props.increaseItem} decreaseItem={this.props.decreaseItem} /> :
                                <button type="button" className="btn btn-info" onClick={this.addToCart}>Add to cart</button>}
                        </div>
                        <div className="col-lg-6 col-md-6 d-flex flex-column mt-2">
                            <h4 className="text-muted">{this.state.productDetails.title}</h4>
                            <h2 className="mt-lg-4 mt-md-4 mt-sm-3 my-2"> Rs. {this.state.productDetails.price}</h2>
                            <div className="col-2 badge bg-warning text-dark my-lg-3 my-2 ">{this.state.productDetails.rating.rate} <img src={staricon} alt="staricon" /></div>
                            <strong>Product Details</strong>
                            <p>{this.state.productDetails.description}</p>
                        </div>
                    </div>
                </div>
        )
    }
}

export default SingleProductPage
