import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import Cart from './Cart'
import Login from './Login'
import accountIcon from "../icons/user.svg"

export class Header extends Component {
    constructor(props) {
        super(props)

        this.state = {
            cartCount: 0,
        }
    }
    componentDidMount() {
        let count = this.props.cartDetails.reduce((count, product) => count += product.quantity, 0)
        this.setState({
            cartCount: count,
        })

    }

    componentDidUpdate() {
        let count = this.props.cartDetails.reduce((count, product) => count += product.quantity, 0)
        if (count !== this.state.cartCount) {
            this.setState({
                cartCount: count,
            })
        }
    }

    render() {
        return (
            <div className="header">
                <Link to="/" className="text-decoration-none text-white"><h2>Fake Store</h2></Link>
                <div className="d-flex">
                    <input className="form-control me-2 d-none d-md-block" type="search" placeholder="Search" aria-label="Search" />
                    <button className="btn btn-dark d-none d-md-block" type="submit">Search</button>
                </div>
                <div>
                    <div className="d-flex">
                        <button className="btn text-dark cart-button" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasExample" aria-controls="offcanvasExample">
                            <div className="button-text fw-bolder">{this.state.cartCount} </div>
                        </button>
                        <button className="btn text-white" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasExample" aria-controls="offcanvasExample">
                            {/* <div className="align-self-center fs-5">Cart</div> */}
                        </button>
                        <Cart cartDetails={this.props.cartDetails} cartCount={this.state.cartCount} increaseItem={this.props.increaseItem} decreaseItem={this.props.decreaseItem} />
                    <div className="account">
                        <button type="button" class="btn login-btn text-white" data-bs-toggle="modal" data-bs-target="#exampleModal">
                            <img src={accountIcon} alt="accountIcon" className="p-1"/>
                        </button>
                        <Login login={this.props.login} logout={this.props.logout}/>
                    </div>
                </div>
                </div>
            </div>
        )
    }
}

export default Header

