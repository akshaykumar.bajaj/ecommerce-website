import axios from 'axios'
import React, { Component } from 'react'
import ErrorPage from './ErrorPage'
import Loader from './Loader'
import ProductCard from './ProductCard'

export class AllProducts extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isLoading: true,
            isError: false,
        }
    }
    componentDidMount() {
        axios.get("https://fakestoreapi.com/products/")
            .then((res) => this.setState({
                productDetails: res.data,
                isLoading: false,
            }))
            .catch((error) => {
                console.log(error)
                this.setState({
                    isLoading: false,
                    isError: true,
                })
            })
    }
    render() {
        return (
            this.state.isLoading ? <Loader /> :
                this.state.isError? <ErrorPage /> :<div className="container">
                    <div className="row justify-content-center align-items-center">
                        {this.state.productDetails.map((product) =>
                            <ProductCard key={product.id} productDetails={product} />
                        )}
                    </div>
                </div>
        )
    }
}


export default AllProducts
