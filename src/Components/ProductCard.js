import React from 'react'
import { Link } from 'react-router-dom'
import staricon from '../star.svg'


function ProductCard(props) {
    return (
        <div className="col-xl-3 offset--xl-0  
                        col-lg-4 offset--lg-0  
                        col-md-6 offset-md-0 
                        col-sm-8 offset-sm-0
                        col-xs-10 offset-xs-1
                        gy-3
                        d-flex justify-content-center">

            <Link to={`/productPage/${props.productDetails.id}`} className = "text-decoration-none text-dark">
                <div className="card product-card">
                    <div className="card-image-wrapper d-flex justify-content-center align-items-center ">
                        <img src={props.productDetails.image} className="card-image product-card-image" alt="productImage"/>
                    </div>
                    <div className="card-body ">
                        <h5 className="card-title">Rs. {props.productDetails.price}</h5>
                        <span className="badge bg-warning text-dark">{props.productDetails.rating.rate} <img src={staricon} alt="staricon" /></span>
                        <p className="card-text">{props.productDetails.title}</p>
                    </div>
                </div>
            </Link>
        </div>
    )
}

export default ProductCard
