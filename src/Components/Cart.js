import React from 'react'

import CartItem from './CartItem'

function Cart(props) {
    return (
        <div>
            <div className="offcanvas offcanvas-end" tabIndex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel">
                <div className="offcanvas-header">
                    <h5 className="offcanvas-title text-dark" id="offcanvasExampleLabel">Cart  Items: {props.cartCount}</h5>
                    <button type="button" className="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                </div>
                <div className="offcanvas-body">
                    {props.cartDetails.map((product) => <CartItem key= {product.productId} product = {product} increaseItem ={props.increaseItem} decreaseItem={props.decreaseItem}/>)}
                </div>
            </div>
        </div>
    )
}

export default Cart
