import React from 'react'

function ErrorPage() {
    return (
        <div>
            <h1 className="display-1">Something Went Wrong! Please Try Again!</h1>
        </div>
    )
}

export default ErrorPage
